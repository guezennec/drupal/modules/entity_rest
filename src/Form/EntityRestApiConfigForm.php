<?php

namespace Drupal\entity_rest\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Entity rest config form.
 */
class EntityRestApiConfigForm extends EntityRestApiForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {

    return 'entity_rest_api_config_form';
  }

  /**
   * Returns an array containing the table headers.
   *
   * @return array
   *   The table header.
   */
  protected function getTableHeader(): array {
    $header = parent::getTableHeader();
    $header['api_name'] = $this->t('Field API name');

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form = parent::buildForm($form, $form_state);

    $form['entity_api_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable API for @type entity type', [
        '@type' => $this->getEntityTypeId() == $this->getBundle() ? $this->getEntityTypeId() : $this->getBundle() . ' ' . $this->getEntityTypeId(),
      ]),
      '#default_value' => $this->getConfig()->get('entity_api_status'),
    ];

    $entity_api_name_description = '
      Type the name that will be exposed on the API path for this particular bundle.<br />
      To be Restful, this should be a plural name.<br />
      If you leave this field empty, The machine name of the bundle prefixed by the entity type id will be used.
    ';
    $form['entity_api_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity API name'),
      '#field_prefix' => \Drupal::request()->getSchemeAndHttpHost() . '/api/',
      '#default_value' => $this->getConfig()->get('entity_api_name') ?? $this->getEntityTypeId() . '-' . $this->getBundle(),
      '#description' => $entity_api_name_description,
      '#weight' => 1,
    ];

    foreach ($this->getFields() as $field) {
      $form['field_list'][$field->getName()]['field_api_names'] = [
        '#type' => 'textfield',
        '#value' => $this->getConfig()->get('field_api_names.' . $field->getName()) ?: $field->getName(),
      ];
    }

    return $form;
  }

  /**
   * @inerhitDoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    if (!$values['entity_api_name']) {
      $form_state->setValue('entity_api_name', $values['entity_type_id'] . '-' . $values['bundle']);
    }

    if (!preg_match('/^[a-z0-9\-]+$/', $form_state->getValue('entity_api_name'))) {
      $form_state->setErrorByName('entity_api_name', $this->t('The entity API name should only contain lowercase characters (<kbd>a-z</kbd>), numbers (<kbd>0-9</kbd>), and hyphens (<kbd>-</kbd>).'));
    }
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $api_names = [];
    foreach ($values['field_list'] as $field_name => $field_values) {
      if ($field_values['field_api_names']) {
        $api_names[$field_name] = $field_values['field_api_names'];
      }
    }

    $editable_config = $this->configFactory->getEditable('entity_rest.api.' . $values['entity_type_id'] . '.' . $values['bundle']);
    $editable_config->set('entity_api_status', $values['entity_api_status'])->save();
    $editable_config->set('entity_api_name', $values['entity_api_name'])->save();
    $editable_config->set('field_api_names', $api_names)->save();

    $this->messenger()->addMessage($this->t('The API configuration for %bundle has been saved', ['%bundle' => $values['bundle']]));
  }

}
