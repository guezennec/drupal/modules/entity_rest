<?php

namespace Drupal\entity_rest\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\entity_rest\Service\EntityRestService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity rest configuration forms.
 */
class EntityRestApiForm extends FormBase {

  /**
   * The route match service.
   * @var \Drupal\Core\Routing\CurrentRouteMatch $route_match
   */
  protected $routeMatch;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity rest service.
   *
   * @var \Drupal\entity_rest\Service\EntityRestService
   */
  protected $entityRestService;

  /**
   * Constructs a new EntityDisplayFormBase.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface|null $entity_field_manager
   *   (optional) The entity field manager.
   * @param \Drupal\entity_rest\Service\EntityRestService $entity_rest_service
   *   The entity rest service.
   */
  public function __construct(CurrentRouteMatch $route_match, EntityFieldManagerInterface $entity_field_manager, EntityRestService $entity_rest_service) {
    $this->routeMatch = $route_match;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityRestService = $entity_rest_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_field.manager'),
      $container->get('entity_rest.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {

    return 'entity_rest_api_form';
  }

  /**
   * Returns an array containing the table headers.
   *
   * @return array
   *   The table header.
   */
  protected function getTableHeader(): array {

    return [
      'label' => $this->t('Label'),
      'name' => $this->t('Machine name'),
      'type' => $this->t('Field type'),
    ];
  }

  /**
   * Get entity_type_id from current route.
   *
   * @return mixed|null
   *   The entity_type_id.
   */
  protected function getEntityTypeId() {

    return $this->routeMatch->getParameter('entity_type_id');
  }

  /**
   * Get bundle from current route.
   *
   * @return mixed|null
   *   The bundle.
   */
  protected function getBundle() {

    return $this->routeMatch->getParameter('bundle');
  }

  /**
   * Get context from current route.
   *
   * @return mixed|null
   *   The context.
   */
  protected function getContext() {

    return $this->routeMatch->getParameter('context');
  }

  /**
   * Get config from current route.
   *
   * @return mixed|null
   *   The context.
   */
  protected function getConfig() {

    return $this->config('entity_rest.api.' . $this->getEntityTypeId() . '.' . $this->getBundle());
  }

  /**
   * Get fields from current route.
   *
   * @return mixed|null
   *   The fields.
   */
  protected function getFields() {

    return $this->entityFieldManager->getFieldDefinitions($this->getEntityTypeId(), $this->getBundle());
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['field_list'] = [
      '#type' => 'table',
      '#header' => $this->getTableHeader(),
      '#empty' => $this->t('No fields found.'),
      '#weight' => 10,
    ];

    foreach ($this->getFields() as $field) {
      $form['field_list'][$field->getName()] = [
        'label' => [
          '#type' => 'item',
          '#markup' => '<strong>' . $field->getLabel() . '</strong>',
        ],
        'name' => [
          '#type' => 'item',
          '#markup' => '<code>' . $field->getName() . '</code>',
        ],
        'type' => [
          '#type' => 'item',
          '#markup' => $field->getType(),
        ],
      ];
    }

    $form['entity_type_id'] = [
      '#type' => 'hidden',
      '#value' => $this->getEntityTypeId(),
    ];

    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $this->getBundle(),
    ];

    $form['context'] = [
      '#type' => 'hidden',
      '#value' => $this->getContext(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 99,
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * @inerhitDoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
