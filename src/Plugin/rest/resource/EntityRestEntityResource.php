<?php

namespace Drupal\entity_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_rest\Service\EntityRestService;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides an Entity Resource
 *
 * @RestResource(
 *   id = "entity_rest_entity_resource",
 *   label = @Translation("Entity Rest Entity Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/{entity_api_name}/{api_identifier}"
 *   }
 * )
 */
class EntityRestEntityResource extends ResourceBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The entity rest service.
   *
   * @var \Drupal\entity_rest\Service\EntityRestService
   */
  protected $entityRestService;

  /**
   * EntityRestEntitiesResource constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\entity_rest\Service\EntityRestService $entity_rest_service
   *   The entity rest service.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    Request $current_request,
    EntityRestService $entity_rest_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $current_request;
    $this->entityRestService = $entity_rest_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_rest.service')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param null $entity_api_name
   *   The custom API name for this entity bundle.
   * @param null $api_identifier
   *   The API identifier of a specific entity.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   */
  public function get($entity_api_name = NULL, $api_identifier = NULL): ResourceResponse {

    $config = $this->entityRestService->getEntityApiConfig($entity_api_name);
    $current_method = strtolower($this->currentRequest->getMethod());

    // If no config file was found or no status or status is 0, return a 404.
    if (!$config || !$config->get('entity_api_status')) {
      throw new NotFoundHttpException();
    }

    // Load entity
    list(, , $entity_type_id) = explode('.', $config->getName());
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity = $storage->loadByProperties(['uuid' => $api_identifier]);
    $entity = reset($entity);

    // If no entity, throw a 404.
    if (!$entity) {
      throw new NotFoundHttpException();
    }

    $fields = $this->entityRestService->getEntityFields($config, $entity, $current_method);

    $response = new ResourceResponse($fields);
    $response->addCacheableDependency($fields);

    return $response;
  }

}
