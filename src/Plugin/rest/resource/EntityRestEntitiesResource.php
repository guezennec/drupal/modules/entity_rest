<?php

namespace Drupal\entity_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_rest\Service\EntityRestService;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides an Entities Resource
 *
 * @RestResource(
 *   id = "entity_rest_entities_resource",
 *   label = @Translation("Entity Rest Entities Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/{entity_api_name}"
 *   }
 * )
 */
class EntityRestEntitiesResource extends ResourceBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The entity rest service.
   *
   * @var \Drupal\entity_rest\Service\EntityRestService
   */
  protected $entityRestService;

  /**
   * EntityRestEntitiesResource constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\entity_rest\Service\EntityRestService $entity_rest_service
   *   The entity rest service.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    Request $current_request,
    EntityRestService $entity_rest_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $current_request;
    $this->entityRestService = $entity_rest_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_rest.service')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param null $entity_api_name
   *   The custom API name for this entity bundle.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($entity_api_name = NULL): ResourceResponse {

    $config = $this->entityRestService->getEntityApiConfig($entity_api_name);
    $current_method = strtolower($this->currentRequest->getMethod());

    // If no config file was found or no status or status is 0, return a 404.
    if (!$config || !$config->get('entity_api_status')) {
      throw new NotFoundHttpException();
    }

    // Load entities and allowed fields for current method
    list(, , $entity_type_id) = explode('.', $config->getName());
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity_ids = $this->entityRestService->getEntities($entity_type_id);
    $entities = [];
    foreach ($storage->loadMultiple($entity_ids) as $entity) {
      $entities[$entity->uuid()] = $this->entityRestService->getEntityFields($config, $entity, $current_method);
    }

    $response = new ResourceResponse($entities);
    $response->addCacheableDependency($entities);

    return $response;
  }

}
