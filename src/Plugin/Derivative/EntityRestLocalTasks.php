<?php

namespace Drupal\entity_rest\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\entity_rest\Service\EntityRestService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides dynamic local tasks for entity rest.
 */
class EntityRestLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity rest service.
   *
   * @var \Drupal\entity_rest\Service\EntityRestService
   */
  protected $entityRestService;

  /**
   * Constructs a new EntityRestLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   * @param \Drupal\entity_rest\Service\EntityRestService $entity_rest_service
   *   The entity rest service.
   */
  public function __construct(
    $base_plugin_id,
    EntityTypeManagerInterface $entity_type_manager,
    TranslationInterface $string_translation,
    EntityRestService $entity_rest_service
  ) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->entityRestService = $entity_rest_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('string_translation'),
      $container->get('entity_rest.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    // Create tabs for all possible entity types.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->get('field_ui_base_route')) {

        $this->derivatives['api_overview_' . $entity_type_id] = [
          'entity_type' => $entity_type_id,
          'title' => $this->t('API'),
          'route_name' => 'entity.' . $entity_type_id . '.api.config',
          'base_route' => $entity_type->get('field_ui_base_route'),
        ];

        $this->derivatives['api_' . $entity_type_id . '_config'] = [
          'entity_type' => $entity_type_id,
          'title' => $this->t('Configuration'),
          'route_name' => 'entity.' . $entity_type_id . '.api.config',
          'parent_id' => 'entity_rest.local_tasks:api_overview_' . $entity_type_id,
        ];

        foreach ($this->entityRestService->getApiContexts() as $context => $label) {
          $this->derivatives['api_' . $entity_type_id . '_' . $context] = [
            'entity_type' => $entity_type_id,
            'title' => $label,
            'route_name' => 'entity.' . $entity_type_id . '.api.' . $context,
            'parent_id' => 'entity_rest.local_tasks:api_overview_' . $entity_type_id,
          ];
        }

      }
    }

    return $this->derivatives;
  }

}
