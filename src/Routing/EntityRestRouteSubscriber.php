<?php

namespace Drupal\entity_rest\Routing;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_rest\Service\EntityRestService;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for entity translation routes.
 */
class EntityRestRouteSubscriber extends RouteSubscriberBase {
  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity rest service.
   *
   * @var \Drupal\entity_rest\Service\EntityRestService
   */
  protected $entityRestService;

  /**
   * Constructs a ContentTranslationRouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\entity_rest\Service\EntityRestService $entity_rest_service
   *   The entity rest service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactory $config_factory,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityRestService $entity_rest_service
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityRestService = $entity_rest_service;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {

      if ($route_name = $entity_type->get('field_ui_base_route')) {

        if (!$entity_route = $collection->get($route_name)) {
          continue;
        }
        $path = $entity_route->getPath();

        $options = $entity_route->getOptions();
        if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
          $options['parameters'][$bundle_entity_type] = [
            'type' => 'entity:' . $bundle_entity_type,
          ];
        }
        // Special parameter used to easily recognize all Field UI routes.
        $options['_field_ui'] = TRUE;

        $defaults = [
          'entity_type_id' => $entity_type_id,
        ];
        // If the entity type has no bundles and it doesn't use {bundle} in its
        // admin path, use the entity type.
        if (strpos($path, '{bundle}') === FALSE) {
          $defaults['bundle'] = !$entity_type->hasKey('bundle') ? $entity_type_id : '';
        }

        $default_title = 'API';

        $route_config = new Route(
          "$path/api",
          [
            '_form' => '\Drupal\entity_rest\Form\EntityRestApiConfigForm',
            '_title' => $default_title,
          ] + $defaults,
          ['_field_ui_form_mode_access' => 'administer ' . $entity_type_id . ' form display'],
          $options
        );
        $collection->add("entity.$entity_type_id.api.config", $route_config);

        foreach (array_keys($this->entityRestService->getApiContexts()) as $context) {
          $route = new Route(
            $path . '/api/' . $context,
            [
              '_form' => '\Drupal\entity_rest\Form\EntityRestApiContextForm',
              '_title' => $default_title,
              'context' => $context,
            ] + $defaults,
            ['_field_ui_form_mode_access' => 'administer ' . $entity_type_id . ' form display'],
            $options
          );
          $collection->add('entity.' . $entity_type_id . '.api.' . $context, $route);
        }

      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    // Should run after AdminRouteSubscriber so the routes can inherit admin
    // status of the edit routes on entities. Therefore, priority -210.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -210];
    return $events;
  }

}
