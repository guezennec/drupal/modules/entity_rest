# Entity REST Drupal module

## TODO

- Separate UI in another module.
- Add view mode like system. The goal is to handle multiple possible output for the API (web front, mobile app...) or different contexts.
- Make api base path configurable. The path is actually hardcoded to "/api".
- Work on caches.
- Use field permissions module to replace own/all contexts.
- Handle post and patch.
- How to handle reference post/patch?